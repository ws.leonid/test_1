<?php
    /*
     * Задача №2:
     * Написать функцию, которая будет принимать
     * натуральное число N и выведет на экран (псевдографика) квадрат со стороной длины N из знаков #, на диагоналях должны
     * быть также значки #, остальные символы – знаки
     * Пример:
     * f(5)
     * #####
     * ##.##
     * #.#.#
     * ##.##
     * #####
     *
     * */
    function  printSquare ($n, $primeSymbol = '#', $otherSymbol = '.') {
        $ai = 1;
        $di = $n;
        $square = $n*$n;
        for ($i=1;$i<=$square;$i++) {
            $symbol = $primeSymbol;
            $line_n = ($i%$n);
            if ($line_n != 1 && $line_n !=0 && $ai != 1 && $di!=1 && $line_n!=$ai && $line_n!=$di ) {
                $symbol = $otherSymbol;
            }

            print $symbol;

            if ($line_n==0) {
                print '<br />';
                $ai++;
                $di--;
            }
        }
    }

    print printSquare(10);
    /*
     * 1. Написать функцию, которая будет принимать натуральное число и возвращать наименование столбца в excel
     * Пример:
     * f(1) должно вернуть A
     * f(3) должно вернуть C
     *
     */





    function ConvertToLetter($n) {
        $str = "";
        While ($n > 0) {
            $a = (int)(($n - 1) / 26);
            $b = ($n - 1) % 26;
            $str = chr($b + 65) . $str;
            $n = $a;
        }
        return $str;
    }
    print ConvertToLetter(2028);


    /*
     *
     * Задача 3. Написать функцию, которая сконвертирует stdClass в
     * ассоциативный массив с наименьшим потреблением памяти
     * (json_encode + json_decode не подойдет).
     *
     * */

    function StdToArray (&$object) {
        $object = (array) $object;
        foreach ($object as $key => &$val) {
            if ($val instanceof stdClass || is_array($val)) {
                $val = StdToArray($val);
            }
        }
        return $object;
    }

    $arr = [
        'to' => [
            'too_latter' => [
                'hei'=> '76786',
                'hh' => 'hh.ru'
            ],
            'hello' => 'hello'
        ],
        'name' => 'vasya',
        'jack' => 'hello jack',
        'haaa' => [
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
                'too_latter' => [
                    'hei'=> '76786',
                    'hh' => 'hh.ru'
                ],
                'hello' => 'hello'
            ],
            [
            'name' => 'vasya',
            'jack' => 'hello jack'
            ]
        ]
    ];
    $arrStd = json_decode(json_encode($arr));

    echo '<br />';

    $startMemory = 0;
    $startMemory = memory_get_usage();
    StdToArray($arrStd);
    var_dump($arrStd);
    echo (memory_get_usage() - $startMemory) . ' bytes' . PHP_EOL;
